#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplfinance as mplf
from sklearn.cluster import KMeans
from sklearn.preprocessing import normalize
from datetime import datetime, timedelta
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


df=pd.read_csv("data/yahoofinance-AAPL-20040819-20180120.csv")


# In[3]:


df.dtypes


# In[4]:



df.Date=df.Date.astype('datetime64[ns]')
df['moyenne'] = (df['Close']+df['High'] + df['Low'])/3


# In[5]:


df = df.set_index(pd.DatetimeIndex(df['Date']))
df.drop('Date', axis=1, inplace=True)
df.head()


# In[6]:


df_30j = df.tail(30)


# In[8]:


# Create figure and plot space
fig, ax = plt.subplots(figsize=(8,8))

# Add x-axis and y-axis
ax.plot(df_30j.index.values,
        df_30j['moyenne'],
        color='purple')

# Set title and labels for axes
ax.set(xlabel="Date",
       ylabel="Valeur moyenne",
       title="Valeur moyenne de l'action Apple - Par Jour")
plt.xticks(rotation=60)
plt.legend(loc="best")
plt.show();


# In[74]:


#OHCL des 30 derniers jours
mplf.plot(df.tail(30), type="candle")

plt.show()


# In[81]:


import requests
from urllib.request import urlopen
from datetime import datetime, timedelta
import time


# In[118]:


from sklearn.preprocessing import scale
def preprocess(df):
    #création des colonnes
    df["moyenne"] = (df["Close"] + df["High"] + df["Low"]) / 3.0
    #normalisation
    df.fillna(0, inplace=True)
    df_norm=scale(df.values)
    df = pd.DataFrame(df_norm, columns=df.columns, index=df.index)
    
    return df


# In[119]:


def telechargement(symbole, derniersJours=30):
    # dates de debut et de fin
    dateFin = datetime.today()
    dateDebut = dateFin - timedelta(days=derniersJours)
    x = int(time.mktime(dateDebut.timetuple()))
    y = int(time.mktime(dateFin.timetuple()))
    url = "https://query1.finance.yahoo.com/v7/finance/download/"+symbole+"?period1="+str(x)+"&period2="+str(y)+"&interval=1d&events=history&crumb=osPYmrnpcIy"
    
    # Lecture des donnees depuis Yahoo finance
    reponse = requests.get(url)
    if reponse.ok == True:
        z = open('csvfile.csv','w')
        z.write(reponse.text)
        z.close()
        DernierMois=pd.read_csv('csvfile.csv', index_col=0, parse_dates=True,infer_datetime_format=True)
        DernierMois = preprocess(DernierMois)
        DernierMois["symbole"] = symbole
        return DernierMois
    else: print("pas de symbole pour :",symbole)


# In[120]:


telechargement("GOOG").head()


# In[121]:


actions_dict={"GOOG": "Google",
            "FB": "Facebook, Inc.",
            "AAPL": "Apple Inc.",
            "MSFT": "Microsoft Corporation",
            "HPQ": "Hewlett-Packard Company",
            "INTC": "Intel Corporation",
            "NVDA": "NVIDIA Corporation",
            "TXN": "Texas Instruments Incorporated",
            "IBM": "International Business Machines Corp. (IBM)",
            "SAP": "SAP SE (ADR)",
            "ADBE": "Adobe Systems Incorporated",
            "ADSK": "Autodesk, Inc.",
            "CRM": "salesforce.com, inc.",
            "N": "NetSuite Inc",
            "VMW": "VMware, Inc.",
            "CTXS": "Citrix Systems, Inc.",
            "RHT": "Red Hat Inc",
            "RAX": "Rackspace Hosting, Inc.",
            "AMZN": "Amazon.com, Inc.",
            "NWSA": "News Corp",
            "EBAY": "eBay Inc",
            "CBS": "CBS Corporation",
            "CMCSA": "Comcast Corporation",
            "VIAB": "Viacom, Inc.",
            "NFLX": "Netflix, Inc.",
            "TWX": "Time Warner Inc",
            "FOXA": "Twenty-First Century Fox Inc",
            "NYT": "The New York Times Company",
            "TRI": "Thomson Reuters Corporation (USA)",
            "DIS": "The Walt Disney Company",
            "SNE": "Sony Corp (ADR)",
            "PCRFY": "Panasonic Corporation (ADR)",
            "CAJ": "Canon Inc (ADR)",
            "TOSYY": "Toshiba Corp (USA)",
            "BBRY": "BlackBerry Ltd",
            "CSC": "Computer Sciences Corporation",
            "GE": "General Electric Company",
            "HTHIY": "Hitachi, Ltd. (ADR)",
            "SIEGY": "Siemens AG (ADR)",
            "CVX": "Chevron Corporation",
            "XOM": "Exxon Mobil Corporation",
            "BP": "BP plc (ADR)",
            "CAT": "Caterpillar Inc.",
            "LXK": "Lexmark International Inc",
            "BKS": "Barnes & Noble, Inc.",
            "FJTSY": "Fujitsu Ltd (ADR)",
            "EMC": "EMC Corporation",
            "ORCL": "Oracle Corporation",
            "CSCO": "Cisco Systems, Inc.",
            "XRX": "Xerox Corp",
           }


# In[122]:


actions_data = pd.DataFrame.from_dict(actions_dict, orient='index')
actions_data = actions_data.reset_index()
actions_data.columns = ['symbole' , 'noms']
actions_data.head()


# In[123]:


#Téléchargement des données
liste = []
for symbole in actions_data["symbole"]:
    temp_data = telechargement(symbole)
    liste.append(temp_data)


# In[124]:


actions_df = pd.concat(liste)
actions_df.head()


# In[125]:


actions_df = actions_df.reset_index()
actions_df.head()


# In[127]:


#Preparartion des données au clustering
def pivot_data(actions_df, values="moyenne"):
    clustering_data = actions_df.pivot(index="symbole", columns="Date", values=values)
    return clustering_data
clustering_data = pivot_data(actions_df, values="moyenne")
clustering_data.head()


# In[128]:


clustering_data = clustering_data.drop(clustering_data.columns[0], axis = 1)
clustering_data.head()


# In[129]:


from sklearn.cluster import KMeans
from sklearn import cluster
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
finance = linkage(clustering_data,method='ward',metric='euclidean')


# In[130]:


plt.figure(figsize=(30, 20))
plt.title('CAH Finance')
plt.xlabel('Moyenne du jour')
plt.ylabel('Noms des entreprises')

dendrogram(finance, labels = list(clustering_data.index), orientation='right', leaf_font_size=18, color_threshold=8)
plt.show()


# In[ ]:




